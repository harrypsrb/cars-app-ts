import React, { Component } from "react";
import { Modal } from "react-bootstrap";

class UiModal extends Component {
  render() {
    return (
      <>
        <Modal
          show={this.props.modalState}
          onHide={() => this.props.modalStateHandler()}
          size="lg"
          backdrop={"static"}
        >
          <Modal.Header closeButton={true}>
            <Modal.Title>{this.props.modalTitle}</Modal.Title>
          </Modal.Header>
          <Modal.Body>{this.props.body}</Modal.Body>
          <Modal.Footer></Modal.Footer>
        </Modal>
      </>
    );
  }
}

export default UiModal;
