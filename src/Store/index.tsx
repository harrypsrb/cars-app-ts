import React from 'react'

interface IState {
    cars: Array<ICar>,
    filteredCars: Array<ICar>,
    carBrand: [],
    carModel: []
}
export interface IAction {
    type: string;
    payload: any
}

export interface ICar {
    queryId?: string|undefined,
    parentQueryId: number | null,
    category?: string,
    name: string,
    query?: string,
    startDate?: string,
    endDate?: string,
    isDeleted?: boolean
    backSearchNeeded?: boolean
    relevance?: number
    isCaseSensitive: boolean
    isStemming: boolean
}

const initialState:IState = {
    cars: [],
    filteredCars: [],
    carBrand: [],
    carModel: []
};

const Store = React.createContext<IState | any>(initialState);
//export const dispatchStore = React.createContext<IState | any>(initialState)

function reducer(state: IState, action: IAction): IState {
    switch (action.type) {
        case "FETCH_DATA":
            return {
                ...state,
                cars: action.payload,
                filteredCars: action.payload,
                carBrand : action.payload.filter((obj: { category: string; }) => obj.category === "brand"),
                carModel : action.payload.filter((obj: { category: string; }) => obj.category === "model")
            };
        case "BRAND_FILTERED":
            let filteredCarModel: ICar[] = [];
            if (action.payload !== "All") {
                filteredCarModel = state.cars.filter(
                    element =>
                        element.category === "model" && element.name
                            .substr(0, action.payload.split("-")[1].trim().length)
                            .trim() === action.payload.split("-")[1].trim()
                );
                //console.log(action.payload);
            } else {
                filteredCarModel = state.cars;
            }

            return {
                ...state,
                filteredCars: filteredCarModel
            };
        case "INPUT_FILTERED":
            let filteredCars: ICar[] = [];
            if (action.payload.model !== "All") {
                filteredCars = state.cars.filter(
                    element => {
                        return element.category === "model" &&
                            element.name.toLowerCase().indexOf(action.payload.filter.toLowerCase()) !== -1 && element.name
                                .substr(0, action.payload.model.split("-")[1].trim().length)
                                .trim() === action.payload.model.split("-")[1].trim()
                    }
                );
            } else {
                filteredCars = state.cars.filter(
                    element => {
                        return element.category === "model" &&
                            element.name.toLowerCase().indexOf(action.payload.filter.toLowerCase()) !== -1
                    }
                );
            }

            return {
                ...state,
                filteredCars: filteredCars
            };
        default:
            return state;
    }
}

function StoreProvider(props: any): JSX.Element {
    const [state, dispatch] = React.useReducer(reducer, initialState)
    return <Store.Provider value={{ state, dispatch}}>{props.children}</Store.Provider>
}

export {Store, StoreProvider}