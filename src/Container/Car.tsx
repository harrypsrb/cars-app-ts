import React from 'react'
import { ICar } from '../Store';

export default function Car(props: any): Array<JSX.Element> {
    const {cars} = props;
    return cars.map((car: ICar, index: number) => {
        return (
            <React.Fragment key={index + 1}>
                <tr
                    key={index + 1}
                    id={car.queryId}
                >
                    <td>{index + 1}</td>
                    <td>{car.name}</td>
                    <td>{car.query}</td>
                    <td>{car.startDate}</td>
                    <td>
                        {car.endDate !== undefined && car.endDate.slice(0, 4) === "2999" ? "-" : car.endDate}
                    </td>
                </tr>
            </React.Fragment>
        );
    })
}
