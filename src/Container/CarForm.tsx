import React, {useContext, useState} from 'react';
import moment from "moment";
import {
    Form,
    Button,
    Row,
    Col,
    Container
} from "react-bootstrap";
import DayPickerInput from "react-day-picker/DayPickerInput";
import InputRange from "react-input-range";
import "react-day-picker/lib/style.css";
import "react-input-range/lib/css/index.css";
import {Store} from "../Store";

export default function CarForm (props: any) {
    const { state, dispatch } = useContext(Store);
    const {brandOption} = props;
    const [modalBrandOption, setModalBrandOption] = useState("");
    const [parentQueryId, setParentQueryId] = useState(0);
    const [modelName, setModelName] = useState("");
    const [query, setQuery] = useState("");
    const [queryValidation, setQueryValidation] = useState(false);
    const [isCaseSensitive, setIsCaseSensitive] = useState(false);
    const [isStemming, setIsStemming] = useState(false);
    const [relevance, setRelevance] = useState(80);
    const [relevanceText, setRelevanceText] = useState("80");

    const optionBrandChangeHandler = (e: any) => {
        setModalBrandOption(e.target.value.split("-")[1].trim());
        setParentQueryId(parseInt(e.target.value.split("-")[0].trim(), 10));

    };

    const modelNameChangeHandler = (e: any) => {
        setModelName(e.target.value);
        setQuery(modalBrandOption + " " + modelName);
    };

    const queryChangeHandler = (e: any) => {
        if (
            e.target.value.substring(0, modalBrandOption.length) === modalBrandOption
        ) {
            setQuery(e.target.value);
            setQueryValidation(false);
        } else {
            setQueryValidation(true);
        }

    };

    const relevanceChangeHandler = (e: any) => {
        setRelevance(e);
        setRelevanceText(e.toString());
    };

    const relevanceTextChangeHandler = (e: any) => {
        if (e.target.value !== ""){
            setRelevance(parseInt(e.target.value));
        }else{
            setRelevance(0);
        }

        setRelevanceText(e.target.value);
    };

    const brandValue =<Form.Group style={{ paddingRight: 10 }} controlId="formGridState">
        <Form.Control
            as="select"
            defaultValue={"Choose..."}
            onChange={e => optionBrandChangeHandler(e)}
        >
            <option disabled={true}>{"Choose..."}</option>
            {brandOption}
        </Form.Control>
    </Form.Group>;

    return (
        <Container>
            <Row>
                <Col id={"columnTitle"}>Select Brand</Col>
                <Col id={"columnValue"}>{brandValue}</Col>
            </Row>
            <Row>
                <Col id={"columnTitle"}>Model Name</Col>
                <Col id={"columnValue"}>
                    <Form.Group
                        style={{ paddingRight: 10 }}
                        controlId="formGridState"
                    >
                        <div style={{ display: "flex", width: "100%" }}>
                            <Form.Group style={{ paddingRight: 10, width: "100%" }}>
                                <Form.Control
                                    disabled
                                    type="text"
                                    value={modalBrandOption}
                                />
                            </Form.Group>
                            <Form.Group style={{ width: "100%" }}>
                                <Form.Control
                                    type="text"
                                    placeholder="Model Name"
                                    onChange={(e: any) => modelNameChangeHandler(e)}
                                    value={modelName}
                                />
                            </Form.Group>
                        </div>
                    </Form.Group>
                </Col>
            </Row>
            <Row style={{ marginTop: "-13px" }}>
                <Col id={"columnTitle"}>Query</Col>
                <Col id={"columnValue"}>
                    <Form.Group
                        style={{ paddingRight: 10 }}
                        controlId="formGridState"
                    >
                        <div style={{ display: "flex", width: "100%" }}>
                            <Form.Group style={{ width: "100%" }}>
                                <Form.Control
                                    type="text"
                                    placeholder="Query"
                                    value={query}
                                    onChange={(e: any) => queryChangeHandler(e)}
                                    isInvalid={queryValidation}
                                />
                                <Form.Control.Feedback type="invalid">
                                    Query must start with brand name.
                                </Form.Control.Feedback>
                            </Form.Group>
                        </div>
                    </Form.Group>
                </Col>
            </Row>
            <Row style={{ marginTop: "-11px", paddingBottom: 30 }}>
                <Col style={{ display: "flex", justifyContent: "space-evenly" }}>
                    <div style={{ paddingRight: 25 }}>
                        <Form.Check
                            inline
                            label="Hoofdletter gevoelig"
                            type={"checkbox"}
                            checked={isCaseSensitive}
                            onChange={(e: any) => setIsCaseSensitive(e.target.checked)}
                        />
                    </div>
                    <div>
                        <Form.Check
                            inline
                            label="Gebruik hoofdletters"
                            type={"checkbox"}
                            checked={isStemming}
                            onChange={(e: any) => setIsStemming(e.target.checked)}
                        />
                    </div>
                </Col>
            </Row>
            <Row>
                <Col id={"columnTitle"} style={{ paddingTop: 1 }}>
                    Relevance
                </Col>
                <Col id={"columnValuesss"} style={{ display: "flex" }}>
                    <InputRange
                        minValue={0}
                        maxValue={100}
                        value={relevance}
                        onChange={(e:any) => relevanceChangeHandler(e)}
                    />
                    <Form.Group
                        style={{ marginLeft: 25, marginTop: -10, width: 60 }}
                        controlId="formGridState"
                    >
                        <div style={{ display: "flex", width: "100%" }}>
                            <Form.Group style={{ width: "100%" }}>
                                <Form.Control
                                    type="text"
                                    onChange={(e: any) => relevanceTextChangeHandler(e)}
                                    value={relevanceText}
                                />
                            </Form.Group>
                        </div>
                    </Form.Group>
                </Col>
            </Row>
            <Row>
                <Col id={"columnTitle"}>Start Date</Col>
                <Col id={"columnValue"}>
                    <DayPickerInput

                    />
                </Col>
                <Col id={"columnTitle"}>End Date</Col>
                <Col id={"columnValue"}>
                    <DayPickerInput

                    />
                </Col>
            </Row>

            <Row style={{ float: "right", padding: " 20px 25px 0 0" }}>
                <Button
                    variant="primary"
                    style={{ width: 140, margin: "20px" }}
                >
                    Save
                </Button>
                <div/>
            </Row>
        </Container>
    )
}