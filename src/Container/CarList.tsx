import React, {useEffect, useContext, useState} from 'react';
import moment from "moment";
import {
    Table,
    Spinner,
    Form,
    Button,
} from "react-bootstrap";
import axios from "axios";
import Modal from "../Component/Modal/Modal";
// import Toast from "../Component/Toaster/index";

import "./CarList.css";

import { Store, ICar } from '../Store';
import CarForm from "./CarForm";

const CarData = React.lazy<any>(() => import('./Car'));

interface ICarProps {
    cars: ICar[],
    car: ICar
}

export default function CarList() {

    const { state, dispatch } = useContext(Store);

    const [filterValue, setFilterValue] = useState("");
    const [selectedModel, setSelectedModel] = useState("All");
    const [modalTitle, setModalTitle] = useState("Add New Car");
    const [modalState, setModalState] = useState(false);
    //const [defaultCar, setDefaultCar] = useState([]);

    useEffect(() => {
        state.cars.length === 0 && FetchDataAction()
    });

    const FetchDataAction = async () => {
        axios
            .get("http://localhost:8080/api/car/crud")
            .then(res => {
                return dispatch({
                    type: 'FETCH_DATA',
                    payload: res.data
                })
            })
            .catch(error => {
                console.warn("Error to retrieve data :" + error);
            });

    };

    const optionBrandFilterHandler = (e: React.ChangeEvent<HTMLSelectElement>) => {
        setSelectedModel(e.target.value);
        setFilterValue("");
        return dispatch({
            type: 'BRAND_FILTERED',
            payload: e.target.value
        });

    };

    const filterHandler = (e: React.ChangeEvent<HTMLInputElement>, selectedModel: string) => {
        setFilterValue(e.target.value);
        //console.log(selectedModel);
        return dispatch({
            type: 'INPUT_FILTERED',
            payload: {
                filter: e.target.value,
                model: selectedModel
            }
        });
    };

    const props: ICarProps = {
        cars: state.filteredCars,
        car: {
            name: "",
            startDate: moment(new Date(), "YYYY-MM-DD").format("YYYY-MM-DD"),
            endDate: "2999-12-31",
            query: "",
            isCaseSensitive: false,
            isStemming: false,
            parentQueryId: 0
        }
    };

    let spinner = (
        <div
            style={{
                padding: 30,
                position: "absolute",
                top: "70%",
                left: "50%",
                transform: "translate(-50%,-50%)"
            }}
        >
            <strong style={{ fontSize: 20, paddingRight: 10 }}>
                Preparing Data
            </strong>
            <Spinner animation="grow" variant="secondary" />
            <Spinner animation="grow" variant="secondary" />
            <Spinner animation="grow" variant="secondary" />
        </div>
    );

    const brandOption = state.carBrand.map((element: { queryId: string; name: {} | null | undefined; }, index: string | number | undefined) => {
        return (
            <React.Fragment key={index}>
                <option value={element.queryId + "-" + element.name}>
                    {element.name}
                </option>
            </React.Fragment>
        );
    });

    const modalStateHandler = () => {
        setModalState(!modalState);
    };

    return (
        <div>
            <React.Suspense fallback={spinner}>
                <div>
                <div className={"formGroupStyle"}>
                    <div className={"filterInput"}>
                        <Form.Group style={{ paddingRight: 10 }} controlId="formGridState">
                            <Form.Control
                                as="select"
                                defaultValue={"All"}
                                onChange={e => optionBrandFilterHandler(e as any)}
                            >
                                <option value={"All"}>All</option>
                                {brandOption}
                            </Form.Control>
                        </Form.Group>
                    </div>
                    <div style={{ width: "20%" }}>
                        <Form.Group>
                            <Form.Control
                                type="text"
                                placeholder="filter..."
                                value={filterValue}
                                onChange={(e: any) => filterHandler(e as any, selectedModel)}
                            />
                        </Form.Group>
                    </div>
                    <div>
                        <Button
                            style={{ position: "absolute", right: "36%" }}
                            name="addButton"
                            onClick={() => modalStateHandler()}
                        >
                            Add New Car
                        </Button>
                    </div>
                </div>
                <div className={"carListTable"}>
                    <Table responsive hover={true} className={"fixed_header"}>
                        <thead>
                        <tr>
                            <th>No</th>
                            <th>Name</th>
                            <th>Query</th>
                            <th>Start Date</th>
                            <th>End Date</th>
                        </tr>
                        </thead>
                        <tbody>
                            <CarData {...props} />
                        </tbody>
                    </Table>

                </div>
                    <Modal
                        modalTitle={modalTitle}
                        modalState={modalState}
                        body={<div><CarForm brandOption={brandOption}/></div>}
                        modalStateHandler={() => modalStateHandler()}
                    />
                </div>
            </React.Suspense>

        </div>
    );
}

