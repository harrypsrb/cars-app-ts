import React from 'react';
import { ToastContainer } from "react-toastify";
import "bootstrap/dist/css/bootstrap.min.css";
import "react-toastify/dist/ReactToastify.min.css";
import './App.css';
import CarList from "./Container/CarList";
import {StoreProvider} from "./Store";

const App: React.FC = () => {
  return (
    <div className="App">
        <StoreProvider>
          <ToastContainer />
          <CarList />
        </StoreProvider>
    </div>
  );
};

export default App;
